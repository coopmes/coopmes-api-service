import { Field, Int, ObjectType } from "@nestjs/graphql";

@ObjectType()
export class User {
    @Field()
    _id: string;

    @Field()
    name: string;

    @Field()
    email: string;

    @Field()
    token: string;

    @Field()
    confirmToken: string;

    @Field(() => [String])
    rooms: any;

    @Field(() => [String])
    friends: any;

    @Field(() => [String])
    notAllowedFriends: any;

    @Field(() => [String])
    bannedUsers: any;

    @Field(() => [String])
    reqToAllowedFriends: any;
}