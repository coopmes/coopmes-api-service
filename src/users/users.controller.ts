import {BadRequestException, Controller, Get, HttpException, Inject, Param} from '@nestjs/common';
import {UsersService} from "./users.service";

@Controller('/users')
export class UsersController {
    constructor(private readonly usersService: UsersService) {}

    @Get('/confirm/:token')
    async userConfirm(@Param() params) {
        const { token } = params;

        const user = await this.usersService.confirmUserRegistration(token);

        const jwtToken = await this.usersService.generateToken(user._id);

        return jwtToken;
    }
}