import {Resolver, Query, Args, Mutation} from "@nestjs/graphql";
import {UsersService} from "./users.service";
import { User } from "./models/user";
import {UsersInput} from "./inputs/users.input";
import {AuthInput} from "./inputs/auth.input";
import {DeleteUserInput} from "./inputs/delete-user.input";
import {Headers, Inject, UseGuards} from "@nestjs/common";
import {GraphqlAuthGuard} from "../auth/local-auth.guard";
import {CurrentUser} from "../decorator/user.decorator";
import {UserToUserInput} from "./inputs/user-to-user.input";
import {UserEmailInput} from "./inputs/user-email.input";
import {GetUserInput} from "./inputs/get-user.input";
import {PubSubEngine} from "graphql-subscriptions";
import {NotificationsResolver} from "../notifications/notifications.resolver";

@Resolver()
export class UsersResolver {
    constructor(
        private readonly usersService: UsersService,
        @Inject('PUB_SUB') private pubSub: PubSubEngine,
        private readonly notificationsResolver: NotificationsResolver
    ) {}

    @Query(() => String)
    async hello() {
        return this.usersService.generateToken('123');
    }

    @Query(() => [User])
    getUsers(@Args('input') input: GetUserInput) {
        return this.usersService.getAll(input.query);
    }

    @Mutation(returns => User)
    async createUser(@Args('input') input: UsersInput) {
        const user = await this.usersService.create(input);

        await this.usersService.sendEmail({
            address: input.email,
            subject: 'Confirm registration',
            text: `${process.env.FRONTEND}/confirmtoken/${user.confirmToken}`,
            html: `<a href="${process.env.FRONTEND}/confirmtoken/${user.confirmToken}">Sign in</a>`
        })

        return user;
    }

    @Query(returns => String)
    async deleteEmailUser(@Args('input') input: DeleteUserInput) {
        return this.usersService.deleteEmailUser(input.email);
    }

    @Mutation(returns => User)
    async createUserGetToken(@Args('input') input: UsersInput) {
        return this.usersService.createAndGenerateToken(input);
    }

    @Query(() => User)
    @UseGuards(GraphqlAuthGuard)
    async getProfile(@CurrentUser() user: User) {
        return user;
    }

    @Query(() => User)
    async auth(@Args('input') input: AuthInput) {
        return this.usersService.authUser(input);
    }

    @Mutation(() => User)
    @UseGuards(GraphqlAuthGuard)
    async addFriend(@Args('input') input: UserEmailInput, @CurrentUser() user: User) {
        const friend = await this.usersService.reqAddFriend(user._id, input.email);

        await this.notificationsResolver.pushEvent('req_friend', {
            friend,
            user
        })

        return friend;
    }

    @Mutation(() => User)
    @UseGuards(GraphqlAuthGuard)
    async removeFriend(@Args('input') input: UserToUserInput, @CurrentUser() user: User) {
        return this.usersService.reqAddFriend(user._id, input.userId)
    }

    @Mutation(() => User)
    @UseGuards(GraphqlAuthGuard)
    allowedFriend(@Args('input') input: UserToUserInput, @CurrentUser() user: User) {
        return this.usersService.allowedFriend(user._id, input.userId);
    }

    @Mutation(() => User)
    @UseGuards(GraphqlAuthGuard)
    notAllowedFriend(@Args('input') input: UserToUserInput, @CurrentUser() user: User) {
        return this.usersService.notAllowedFriend(user._id, input.userId);
    }
}
