import { ObjectType, Field, Int, ID } from 'type-graphql';

@ObjectType()
export class UserTypeDto {
    @Field()
    readonly id: string;
    @Field()
    readonly name: string;
    @Field()
    readonly email: string;
    @Field()
    readonly password: string;
}