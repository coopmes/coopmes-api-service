import { InputType, Field, Int } from '@nestjs/graphql';

@InputType()
export class GetUserInput {
    @Field({
        defaultValue: ''
    })
    readonly query: string;

    // @Field()
    // readonly users: string;
}