import { InputType, Field, Int } from '@nestjs/graphql';

@InputType()
export class UserEmailInput {
    @Field(type => String)
    readonly email: string;
}