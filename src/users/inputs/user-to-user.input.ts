import { InputType, Field, Int } from '@nestjs/graphql';

@InputType()
export class UserToUserInput {
    @Field(type => String)
    readonly userId: string;
}