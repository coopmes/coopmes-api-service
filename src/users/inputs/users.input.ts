import { InputType, Field, Int } from '@nestjs/graphql';

@InputType()
export class UsersInput {
    @Field(type => String)
    readonly name: string;
    @Field(type => String)
    readonly email: string;
    @Field(type => String)
    readonly password: string;
}