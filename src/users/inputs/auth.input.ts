import { InputType, Field, Int } from '@nestjs/graphql';

@InputType()
export class AuthInput {
    @Field(type => String)
    readonly login: string;
    @Field(type => String)
    readonly password: string;
}