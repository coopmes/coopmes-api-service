import { InputType, Field, Int } from '@nestjs/graphql';

@InputType()
export class DeleteUserInput {
    @Field(type => String)
    readonly email: string;
}