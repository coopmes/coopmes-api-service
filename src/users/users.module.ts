import {Module} from '@nestjs/common';
import {UsersResolver} from './users.resolver';
import {UsersService} from './users.service';
import {ClientsModule, Transport} from "@nestjs/microservices";
import {UsersController} from "./users.controller";
import {AppController} from "../app.controller";
import {PubSub} from "graphql-subscriptions";
import {NotificationsResolver} from "../notifications/notifications.resolver";

@Module({
    imports: [
        ClientsModule.register([
            {
                name: 'AUTH_SERVICE',
                transport: Transport.NATS,
                options: {
                    url: process.env.NATS,
                }
            },
        ]),
        ClientsModule.register([
            {
                name: 'MESSENGER_SERVICE',
                transport: Transport.NATS,
                options: {
                    url: process.env.NATS,
                }
            },
        ]),
        ClientsModule.register([
            {
                name: 'NOTIFIER_SERVICE',
                transport: Transport.NATS,
                options: {
                    url: process.env.NATS,
                }
            },
        ]),
    ],
    controllers: [UsersController],
    providers: [
        UsersResolver,
        UsersService,
        NotificationsResolver,
        {
            provide: 'PUB_SUB',
            useValue: new PubSub(),
        },
    ]
})
export class UsersModule {
}
