import {HttpException, Inject, Injectable, InternalServerErrorException, UnauthorizedException} from '@nestjs/common';
import {ClientProxy} from "@nestjs/microservices";
import {UsersInput} from "./inputs/users.input";
import {AuthInput} from "./inputs/auth.input";
import {SendEmailDto} from "./dto/send-email.dto";

@Injectable()
export class UsersService {
    constructor(
        @Inject('AUTH_SERVICE') private auth: ClientProxy,
        @Inject('MESSENGER_SERVICE') private messenger: ClientProxy,
        @Inject('NOTIFIER_SERVICE') private notifier: ClientProxy
    ) {}

    getAll(query: string) {
        const pattern = { cmd: 'get_users' };
        return this.messenger.send(pattern, {query});
    }

    create(createDto: UsersInput) {
        const pattern = { cmd: 'add_user' };
        return this.messenger.send(pattern, createDto).toPromise();
    }

    generateToken(userId: string) {
        const pattern = { cmd: 'generate_token' };
        return this.auth.send(pattern, {
            userId
        }).toPromise()
    }

    getUserIdFromToken(token: string) {
        const pattern = { cmd: 'get_user_id' };
        return this.auth.send(pattern, token).toPromise()
    }

    async getUserFromToken(token: string) {
        const userId = await this.getUserIdFromToken(token);

        if (!userId) {
            throw new UnauthorizedException;
        }

        return this.messenger.send({ cmd: 'find_user_by_id' }, userId).toPromise()
    }

    validatePassword(fields: AuthInput) {
        const pattern = { cmd: 'email_login' }
        return this.messenger.send(pattern, {
            email: fields.login,
            password: fields.password
        }).toPromise()
    }

    confirmUserRegistration(token: string) {
        return this.messenger.send({ cmd: 'confirm_auth_user' }, {token}).toPromise()
    }

    async authUser(fields: AuthInput) {
        const user = await this.validatePassword(fields);

        if (!user) {
            throw new UnauthorizedException;
        }

        if (!user.confirmAuth) {
            throw new UnauthorizedException;
        }

        const token = await this.generateToken(user._id);

        return {...user, token}
    }

    async createAndGenerateToken(fields: UsersInput) {
        const user = await this.create(fields);

        if (!user) {
            throw new InternalServerErrorException;
        }

        const token = await this.generateToken(user._id);

        return {...user, token};
    }

    sendEmail(fields: SendEmailDto) {
        return this.notifier.send({ cmd: 'email_send' }, fields).toPromise()
    }

    deleteEmailUser(email: string) {
        return this.messenger.send({ cmd: 'delete_email_user' }, email).toPromise()
    }

    reqAddFriend(id: string, friendEmail: string) {
        return this.messenger.send({ cmd: 'add_friend' }, {id, friendEmail});
    }

    removeFriend(id: string, friendId: string) {
        return this.messenger.send({ cmd: 'remove_friend' }, {id, friendId});
    }

    allowedFriend(id: string, friendId: string) {
        return this.messenger.send({ cmd: 'allowed_friend' }, {id, friendId});
    }

    notAllowedFriend(id: string, friendId: string) {
        return this.messenger.send({ cmd: 'not_allowed_friend' }, {id, friendId});
    }
}
