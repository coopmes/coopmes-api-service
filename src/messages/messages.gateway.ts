import { WebSocketGateway, WebSocketServer, SubscribeMessage, OnGatewayConnection, OnGatewayDisconnect } from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import {UseGuards} from "@nestjs/common";
import {GraphqlAuthGuard} from "../auth/local-auth.guard";

@WebSocketGateway()
export class MessagesGateway implements OnGatewayConnection, OnGatewayDisconnect {

    @WebSocketServer() server;
    users: number = 0;

    async handleConnection(){
        // A client has connected
        this.users++;

        console.log(this.users);

        // Notify connected clients of current users
        this.server.emit('users', this.users);

    }

    async handleDisconnect(){

        // A client has disconnected
        this.users--;

        // Notify connected clients of current users
        this.server.emit('users', this.users);
    }

    @SubscribeMessage('msgToClient')
    @UseGuards(GraphqlAuthGuard)
    handleMessage(message): void {
        this.server.emit(message.roomId, JSON.stringify(message));
    }
}
