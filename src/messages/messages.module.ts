import { Module } from '@nestjs/common';
import { MessagesService } from './messages.service';
import {ClientsModule, Transport} from "@nestjs/microservices";
import {UsersService} from "../users/users.service";
import {MessagesResolver} from "./messages.resolver";
import {MessagesGateway} from "./messages.gateway";
import { PubSub } from 'graphql-subscriptions';


@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'MESSENGER_SERVICE',
        transport: Transport.NATS,
        options: {
          url: process.env.NATS,
        }
      },
    ]),
    ClientsModule.register([
      {
        name: 'AUTH_SERVICE',
        transport: Transport.NATS,
        options: {
          url: process.env.NATS,
        }
      },
    ]),
    ClientsModule.register([
      {
        name: 'NOTIFIER_SERVICE',
        transport: Transport.NATS,
        options: {
          url: process.env.NATS,
        }
      },
    ]),
  ],
  providers: [
      MessagesService,
      UsersService,
      MessagesResolver,
      MessagesGateway,
    {
      provide: 'PUB_SUB',
      useValue: new PubSub(),
    },
  ]
})
export class MessagesModule {}
