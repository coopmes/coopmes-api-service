import { Field, Int, ObjectType } from "@nestjs/graphql";

@ObjectType()
export class Message {
    @Field()
    _id: string;

    @Field(type => String)
    readonly text: string;

    @Field(type => String)
    readonly roomId: string;

    @Field(type => String)
    readonly userId: string;

    @Field(type => String)
    readonly createAt: string;
}