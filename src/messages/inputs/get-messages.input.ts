import { InputType, Field, Int } from '@nestjs/graphql';
import {PaginatorInput} from "../../input/paginator.input";

@InputType()
export class GetMessagesInput extends PaginatorInput {
    @Field(type => String)
    readonly roomId: string;
}