import { InputType, Field, Int } from '@nestjs/graphql';

@InputType()
export class SubMessagesInput {
    @Field(type => String)
    readonly roomId: string;
}