import { InputType, Field, Int } from '@nestjs/graphql';

@InputType()
export class PushMessageInput {
    @Field(type => String)
    readonly text: string;

    @Field(type => String)
    readonly roomId: string;
}