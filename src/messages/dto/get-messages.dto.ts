import {IsEmail, IsNotEmpty} from "class-validator";

export class GetMessagesDto {
    @IsNotEmpty()
    roomId: string;

    @IsNotEmpty()
    userId: string;

    @IsNotEmpty()
    offset: number;

    @IsNotEmpty()
    limit: number;
}
