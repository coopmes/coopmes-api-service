import {IsEmail, IsNotEmpty} from "class-validator";

export class PushMessageDto {
    @IsNotEmpty()
    text: string;

    @IsNotEmpty()
    roomId: string;

    @IsNotEmpty()
    userId: string;
}
