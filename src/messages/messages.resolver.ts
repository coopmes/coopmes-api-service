import {Args, Mutation, Query, Resolver, Subscription} from "@nestjs/graphql";
import {User} from "../users/models/user";
import {Inject, UseGuards} from "@nestjs/common";
import {GraphqlAuthGuard} from "../auth/local-auth.guard";
import {CurrentUser} from "../decorator/user.decorator";
import {GetMessagesInput} from "./inputs/get-messages.input";
import {MessagesService} from "./messages.service";
import {Message} from "./models/message";
import {PushMessageInput} from "./inputs/push-message.input";
import {MessagesGateway} from "./messages.gateway";
import { PubSubEngine } from 'graphql-subscriptions';
import {SubMessagesInput} from "./inputs/sub-messages.input";


@Resolver()
export class MessagesResolver {
    constructor(
        private readonly messagesService: MessagesService,
        private readonly messagesGateWay: MessagesGateway,
        @Inject('PUB_SUB') private pubSub: PubSubEngine
    ) {}

    @Query(() => [Message])
    @UseGuards(GraphqlAuthGuard)
    getMessages(@Args('input') input: GetMessagesInput, @CurrentUser() user: User) {
        return this.messagesService.getMessages({
            roomId: input.roomId,
            userId: user._id,
            offset: input.offset,
            limit: input.limit
        });
    }

    @Subscription(returns => Message, {
        name: 'messagePushed',
        filter(this: MessagesResolver, payload, variables) {
            return payload.messagePushed.roomId === variables.input.roomId;
        }
    })
    handlerAddMessage(@Args('input') input: SubMessagesInput) {
        return this.pubSub.asyncIterator('messagePushed');
    }

    @Mutation(returns => Message)
    @UseGuards(GraphqlAuthGuard)
    async pushMessage(@Args('input') input: PushMessageInput, @CurrentUser() user: User) {
        const message = await this.messagesService.pushMessage({
            text: input.text,
            roomId: input.roomId,
            userId: user._id
        });

        this.pubSub.publish('messagePushed', { messagePushed: message })

        return message;
    }
}