import {Inject, Injectable} from '@nestjs/common';
import {ClientProxy} from "@nestjs/microservices";
import {GetMessagesDto} from "./dto/get-messages.dto";
import {PushMessageDto} from "./dto/push-message.dto";

@Injectable()
export class MessagesService {
    constructor(
        @Inject('MESSENGER_SERVICE') private messenger: ClientProxy,
    ) {
    }

    getMessages(messageDto: GetMessagesDto) {
        return this.messenger.send({ cmd: 'get_messages' }, messageDto);
    }

    pushMessage(pushMessageDto: PushMessageDto) {
        return this.messenger.send({ cmd: 'push_message' }, pushMessageDto).toPromise();
    }
}
