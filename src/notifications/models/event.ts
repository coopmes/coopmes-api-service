import { Field, Int, ObjectType } from "@nestjs/graphql";

@ObjectType()
export class Event {
    @Field(type => String)
    readonly text: string;

    @Field(type => String)
    readonly type: string;
}