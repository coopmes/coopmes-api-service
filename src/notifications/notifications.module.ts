import {Module} from '@nestjs/common';
import {NotificationsResolver} from './notifications.resolver';
import {PubSub} from "graphql-subscriptions";
import {ClientsModule, Transport} from "@nestjs/microservices";
import {UsersService} from "../users/users.service";

@Module({
    imports: [
        ClientsModule.register([
            {
                name: 'AUTH_SERVICE',
                transport: Transport.NATS,
                options: {
                    url: 'nats://localhost:4222',
                }
            },
        ]),
        ClientsModule.register([
            {
                name: 'MESSENGER_SERVICE',
                transport: Transport.NATS,
                options: {
                    url: 'nats://localhost:4222',
                }
            },
        ]),
        ClientsModule.register([
            {
                name: 'NOTIFIER_SERVICE',
                transport: Transport.NATS,
                options: {
                    url: 'nats://localhost:4222',
                }
            },
        ]),
    ],
    providers: [
        NotificationsResolver,
        UsersService,
        {
            provide: 'PUB_SUB',
            useValue: new PubSub(),
        },
    ]
})
export class NotificationsModule {
}
