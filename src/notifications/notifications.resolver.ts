import {Args, Resolver, Subscription} from '@nestjs/graphql';
import {SubMessagesInput} from "../messages/inputs/sub-messages.input";
import {Inject, UseGuards} from "@nestjs/common";
import {PubSubEngine} from "graphql-subscriptions";
import {Event} from "./models/event";
import {GraphqlAuthGuard} from "../auth/local-auth.guard";
import {CurrentUser} from "../decorator/user.decorator";
import {User} from "../users/models/user";
import {AuthSubInput} from "../input/auth.input";
import {UsersService} from "../users/users.service";

@Resolver()
export class NotificationsResolver {
    constructor(
        private usersService: UsersService,
        @Inject('PUB_SUB') private pubSub: PubSubEngine
    ) {
    }

    @Subscription(returns => Event, {
        name: 'event',
        filter(this: NotificationsResolver, payload, variables) {
            console.log(payload);
            console.log(variables);

            return true;
        }
    })
    async handlerEvent(@Args('input') input: AuthSubInput) {
        const profile = await this.usersService.getUserFromToken(input.token);

        if (!profile) {
            return false;
        }

        return this.pubSub.asyncIterator('event');
    }

    async pushEvent(type: string, fields) {
        console.log('pushEvent');

        return this.pubSub.publish('event', {
            type,
            ...fields
        })
    }
}
