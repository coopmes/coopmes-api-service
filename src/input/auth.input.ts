import { InputType, Field, Int } from '@nestjs/graphql';

@InputType()
export class AuthSubInput {
    @Field(returns => String)
    readonly token: string;
}