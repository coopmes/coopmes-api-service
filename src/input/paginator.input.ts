import { InputType, Field, Int } from '@nestjs/graphql';

@InputType()
export class PaginatorInput {
    @Field({
        defaultValue: 0
    })
    readonly offset: number;

    @Field({
        defaultValue: 50
    })
    readonly limit: number;
}