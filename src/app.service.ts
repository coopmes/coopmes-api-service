import {Inject, Injectable, UnauthorizedException} from '@nestjs/common';
import {ClientProxy} from "@nestjs/microservices";

@Injectable()
export class AppService {
  constructor(
      @Inject('AUTH_SERVICE') private auth: ClientProxy,
      @Inject('MESSENGER_SERVICE') private messenger: ClientProxy
  ) {}

  getUserIdFromToken(token: string) {
    const pattern = { cmd: 'get_user_id' };
    return this.auth.send(pattern, token).toPromise()
  }

  async getUserFromToken(token: string) {
    try {
      const userId = await this.getUserIdFromToken(token);

      if (!userId) {
        throw new UnauthorizedException;
      }

      return this.messenger.send({ cmd: 'find_user_by_id' }, userId).toPromise()
    } catch (e) {
      return null;
    }
  }
}
