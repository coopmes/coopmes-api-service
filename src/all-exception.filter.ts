import {Catch, ArgumentsHost, HttpStatus, HttpException} from '@nestjs/common';
import {BaseRpcExceptionFilter, RpcException} from '@nestjs/microservices';
import {GqlArgumentsHost, GqlExceptionFilter, GqlExecutionContext} from "@nestjs/graphql";
import {ExecutionContextHost} from "@nestjs/core/helpers/execution-context-host";

@Catch()
export class AllExceptionsFilter implements GqlExceptionFilter {
    catch(exception: any, host: ExecutionContextHost) {
        // const ctx = host.switchToHttp();
        // const response = ctx.getResponse();
        // const request = ctx.getRequest();
        // const next = ctx.getNext();

        throw new RpcException(exception.message);
    }
}