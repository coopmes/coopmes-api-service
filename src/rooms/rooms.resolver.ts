import {Args, Mutation, Query, Resolver, Subscription} from "@nestjs/graphql";
import {RoomsService} from "./rooms.service";
import {User} from "../users/models/user";
import {Inject, UseGuards} from "@nestjs/common";
import {GraphqlAuthGuard} from "../auth/local-auth.guard";
import {CurrentUser} from "../decorator/user.decorator";
import {Room} from "./models/room";
import {CreateRoomInput} from "./inputs/create-room.input";
import {PutRoomInput} from "./inputs/put-room.input";
import {GetRoomsInput} from "./inputs/get-rooms.input";
import {Message} from "../messages/models/message";
import {SubMessagesInput} from "../messages/inputs/sub-messages.input";
import {PubSubEngine} from "graphql-subscriptions";

@Resolver()
export class RoomsResolver {
    constructor(
        private readonly roomsService: RoomsService,
        @Inject('PUB_SUB') private pubSub: PubSubEngine
    ) {}

    @Query(() => [Room])
    @UseGuards(GraphqlAuthGuard)
    async getRooms(@CurrentUser() user: User, @Args('input') input: GetRoomsInput) {
        let rooms = await this.roomsService.getRooms({
            userId: user._id,
            limit: input.limit,
            offset: input.offset
        });

        rooms = rooms.map(r => {
            if (!r.lastMessageDate) {
                r.lastMessageDate = '';
            }

            return r;
        })

        return rooms;
    }

    @Subscription(returns => Room, {
        name: 'roomCreated',
        filter(this: RoomsResolver, payload, variables) {

            console.log(variables);

            return payload.messagePushed.roomId === variables.input.roomId;
        }
    })
    addCommentHandler(@Args('input') input: SubMessagesInput) {
        return this.pubSub.asyncIterator('roomCreated');
    }

    @Mutation(returns => Room)
    @UseGuards(GraphqlAuthGuard)
    createRoom(@Args('input') input: CreateRoomInput, @CurrentUser() user: User) {
        return this.roomsService.createRoom({
            name: input.name,
            users: input.users,
            userId: user._id
        });
    }

    @Mutation(returns => Room)
    @UseGuards(GraphqlAuthGuard)
    putRoom(@Args('input') input: PutRoomInput, @CurrentUser() user: User) {
        return this.roomsService.putRoom({
            id: input.id,
            name: input.name,
            users: input.users,
            administrators: input.administrators,
            userId: user._id
        });
    }
}