export interface GetRoomInterface {
    userId: string,
    offset: number,
    limit: number
}