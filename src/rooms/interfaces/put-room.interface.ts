export interface PutRoomInterface {
    id: string;
    users: string[];
    administrators: string[];
    userId: string;
    name: string;
}