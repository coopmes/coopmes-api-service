export interface CreateRoomInterface {
    name: string,
    users: string[],
    userId: string
}
