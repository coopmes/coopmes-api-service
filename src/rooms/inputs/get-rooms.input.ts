import { InputType, Field, Int } from '@nestjs/graphql';
import {PaginatorInput} from "../../input/paginator.input";

@InputType()
export class GetRoomsInput extends PaginatorInput {

}