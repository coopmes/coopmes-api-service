import { InputType, Field, Int } from '@nestjs/graphql';

@InputType()
export class CreateRoomInput {
    @Field(type => String)
    readonly name: string;

    @Field(type => [String])
    readonly users: string[];
}