import { InputType, Field, Int } from '@nestjs/graphql';

@InputType()
export class PutRoomInput {
    @Field(type => String)
    readonly id: string;

    @Field(type => String)
    readonly name: string;

    @Field(type => [String])
    readonly users: string[];

    @Field(type => [String])
    readonly administrators: string[];

    @Field(type => String)
    readonly userId: string;
}