import {Inject, Injectable} from '@nestjs/common';
import {ClientProxy} from "@nestjs/microservices";
import {CreateRoomInterface} from "./interfaces/create-room.interface";
import {PutRoomInterface} from "./interfaces/put-room.interface";
import {GetRoomInterface} from "./interfaces/get-room.interface";

@Injectable()
export class RoomsService {
    constructor(
        @Inject('MESSENGER_SERVICE') private messenger: ClientProxy,
    ) {
    }

    createRoom(room: CreateRoomInterface) {
        return this.messenger.send({ cmd: 'create_room' }, room);
    }

    putRoom(room: PutRoomInterface) {
        return this.messenger.send({ cmd: 'put_room' }, room);
    }

    getRooms(room: GetRoomInterface) {
        return this.messenger.send({ cmd: 'get_rooms' }, room).toPromise()
    }
}
