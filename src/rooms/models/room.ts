import { Field, Int, ObjectType } from "@nestjs/graphql";

@ObjectType()
export class Room {
    @Field()
    _id: string;

    @Field(type => String)
    readonly name: string;

    @Field(type => [String])
    readonly users: string[];

    @Field(type => [String])
    readonly administrators: string[];

    @Field(type => String)
    readonly userId: string;

    @Field(type => String)
    readonly lastMessage: string;

    @Field(type => String)
    readonly lastMessageDate: string;
}