import { Module } from '@nestjs/common';
import { RoomsService } from './rooms.service';
import {RoomsResolver} from "./rooms.resolver";
import {ClientsModule, Transport} from "@nestjs/microservices";
import {UsersService} from "../users/users.service";
import {PubSub} from "graphql-subscriptions";

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'MESSENGER_SERVICE',
        transport: Transport.NATS,
        options: {
          url: process.env.NATS,
        }
      },
    ]),
    ClientsModule.register([
      {
        name: 'AUTH_SERVICE',
        transport: Transport.NATS,
        options: {
          url: process.env.NATS,
        }
      },
    ]),
    ClientsModule.register([
      {
        name: 'NOTIFIER_SERVICE',
        transport: Transport.NATS,
        options: {
          url: process.env.NATS,
        }
      },
    ]),
  ],
  providers: [
      RoomsService,
      RoomsResolver,
      UsersService,
    {
      provide: 'PUB_SUB',
      useValue: new PubSub(),
    },
  ]
})
export class RoomsModule {}
