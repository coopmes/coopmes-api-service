import { ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GqlExecutionContext } from '@nestjs/graphql';
import {UsersService} from "../users/users.service";

@Injectable()
export class GraphqlAuthGuard extends AuthGuard('jwt') {
    constructor(
        private usersService: UsersService
    ) {
        super();
    }

    async canActivate(context: ExecutionContext) {
        const ctx = GqlExecutionContext.create(context);
        const { req } = ctx.getContext();

        const { token } = req.headers;

        if (!token) {
            return false;
        }

        try {
            const user = await this.getUser(token);

            if (!user) {
                return false;
            }

            req.user = user;

            return true;
        } catch (e) {
            console.log(e);

            return false;
        }
    }

    getUser(token: string) {
        return this.usersService.getUserFromToken(token)
    }
}