import { Module } from '@nestjs/common';
import {UsersService} from "../users/users.service";
import {ClientsModule, Transport} from "@nestjs/microservices";

@Module({
    imports: [
        ClientsModule.register([
            {
                name: 'AUTH_SERVICE',
                transport: Transport.NATS,
                options: {
                    url: 'nats://localhost:4222',
                }
            },
        ]),
        ClientsModule.register([
            {
                name: 'MESSENGER_SERVICE',
                transport: Transport.NATS,
                options: {
                    url: 'nats://localhost:4222',
                }
            },
        ]),
        ClientsModule.register([
            {
                name: 'NOTIFIER_SERVICE',
                transport: Transport.NATS,
                options: {
                    url: 'nats://localhost:4222',
                }
            },
        ]),
    ],
    providers: [UsersService],
})
export class AuthModule {}
